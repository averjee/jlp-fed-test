import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import productData from '../mockData/data.json';
import Home, { getServerSideProps } from "./index";



describe('Overall Product Grid', () => {

    beforeEach(() => {
        fetch.resetMocks();
    });


	it('Should load all the products', async () => {
        fetch.mockResponseOnce(JSON.stringify(productData));
        const response = await getServerSideProps();
        expect(fetch).toHaveBeenCalledTimes(1);
        expect(response).toEqual({
            props: {
                error: false,
                data: productData
            }
        });
    });

	it('When there are no items, show a message indicating there are no dishwashers', async () => {
		render(<Home data={{products: []}} />);
		const errorMessage = screen.getByText('There are no dishwashers');
		expect(errorMessage).toBeInTheDocument();
	})


	it('If api returns an error we should show an error message', async () => {
		// response needs to be a json otherwise it will cause an error
		fetch.mockResponseOnce('hello world');
		const response = await getServerSideProps();
		expect(response).toEqual({
			props: {
				error: true,
				data: false
			}
		});

		render(<Home error />);
		const errorMessage = screen.getByText('Unfortunately we have encounted an error, please try again');
		expect(errorMessage).toBeInTheDocument();
	});


});