import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import data from '../../mockData/data2.json';
import ProductDetail, { getServerSideProps } from "./[id]";

describe('ProductDetail', () => {

	beforeEach(() => {
		fetch.resetMocks();
	});

	it('Loads the product detail', async () => {
		fetch.mockResponseOnce(JSON.stringify(data));
		const response = await getServerSideProps({params: {id: 1}});
		expect(response).toEqual({
			props: {
				error: false,
				data: data
			}
		});
	});

	it('Should show the correct price', async () => {
		render(<ProductDetail data={data} />);
		const price = screen.getAllByText(`£${data.price.now}`);
		expect(price[0]).toBeInTheDocument();
	});

	it('On error should show a message', async () => {
		fetch.mockResponseOnce('hello world');
		const response = await getServerSideProps({params: {id: 1}});
		expect(response).toEqual({
			props: {
				error: true,
			}
		});

		render(<ProductDetail error />);
		const errorMessage = screen.getByText('Unfortunately, we have encounted an error loading this product. Please try again.');
		expect(errorMessage).toBeInTheDocument();
	});

	
});
