import ProductCarousel from "../../components/product-carousel/product-carousel";
import styles from "./product-detail.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import {PRODUCT_DETAIL_URL,HEADERS} from "../../components/constants/api";

export async function getServerSideProps(context) {
  const id = context.params.id;
  try {
    const response = await fetch(PRODUCT_DETAIL_URL(id),HEADERS);
    const data = await response.json();
    return {
      props: { data, error: false },
    };
  } catch (e) {
    return {
      props: {
        error: true,
      },
    };
  }
}

const ProductDetail = ({ data, error }) => {
  if (error) {
    return (
      <p>
        Unfortunately, we have encounted an error loading this product. Please
        try again.
      </p>
    );
  }

  const pricing = (
    <>
      <div className={styles.price}>£{data.price.now}</div>
      <div className={styles.special}>{data.displaySpecialOffer}</div>
      <div className={styles.included}>
        {data.additionalServices.includedServices}
      </div>
    </>
  );

  return (
    <>
      <div>
        <div className={styles.arrowLeft}>
          <Link key={'Home'} href={{ pathname: "/" }}>
            <FontAwesomeIcon icon={faAngleLeft} size={"xl"} />
          </Link>
        </div>
        <div className={styles.itemTitle}>
          <h1>{data.title}</h1>
        </div>
      </div>
      <div className={styles.page}>
        <div className={styles.productLeft}>
          <ProductCarousel image={data.media.images.urls} />
          <article className={styles.information}>
            <h3 className={styles.pricingPanelMobile}>{pricing}</h3>
            <div>
              <h3>Product information</h3>
              <div
                dangerouslySetInnerHTML={{
                  __html: data.details.productInformation,
                }}
              />
            </div>
            <div>
              <p>Product code: {data.defaultSku}</p>
            </div>
            <h3>Product specification</h3>
            <ul className={styles.list}>
              {data.details.features[0].attributes.map((item, i) => (
                <li key={i} className={styles.listItem}>
                  <div>{item.name}</div>
                  <div>{item.value}</div>
                </li>
              ))}
            </ul>
          </article>
        </div>
        <div className={styles.productRight}>
          <h3 className={styles.pricingPanelDesktop}>{pricing}</h3>
        </div>
      </div>
    </>
  );
};

export default ProductDetail;
