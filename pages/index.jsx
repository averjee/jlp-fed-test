import Head from "next/head";
import styles from "./index.module.scss";
import ProductListItem from "../components/product-list-item/product-list-item";
import {PRODUCT_URL,HEADERS} from "../components/constants/api";

export async function getServerSideProps() {
  try {
    const response = await fetch(PRODUCT_URL,HEADERS);
    const data = await response.json();
    return {
      props: {
        data: data,
        error: false,
      },
    };
  } catch (e) {
    return {
      props: {
        data: false,
        error: true,
      },
    };
  }
}


  const Home = ({ data, error }) => {
    return (
      <div>
        <Head>
          <title>JL &amp; Partners | Home</title>
          <meta name="keywords" content="shopping" />
        </Head>
        <div>
  
          {error && (
            <p>Unfortunately we have encounted an error, please try again</p>
          )}
  
          {!error && data?.products.length > 0 && (
            <>
              <h1>Dishwashers ({data.products.length})</h1>
              <div className={styles.content}>
                {data.products.map((item, i) => (
                  <ProductListItem
                    key={i}
                    id={item.productId}
                    image={item.image}
                    title={item.title}
                    price={item.variantPriceRange.display.max}
                  />
                ))}
              </div>
            </>
          )}
  
          {!error && data?.products.length === 0 && (
            <h1>There are no dishwashers</h1>
          )}
        </div>
      </div>
    );
  };
  

export default Home;
