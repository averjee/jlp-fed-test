import { Carousel } from "react-responsive-carousel";
import styles from "./product-carousel.module.scss";
import "react-responsive-carousel/lib/styles/carousel.min.css";

const ProductCarousel = ({ image }) => {
  	let images = image.map((image, i) => {
		return (
			<div key={i}>
				<img src={image} alt="" style={{ width: "100%", maxWidth: "500px" }} />
			</div>
		);
	});

	return (
		<div className={styles.productCarousel}>
			<Carousel renderThumbs={() => {}} showStatus={false}>
				{images}
			</Carousel>
		</div>
	);
};

export default ProductCarousel;
