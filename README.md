# Submitted by Alnoor Verjee

## Notes including Fixes

- Had to downgrade `@testing-library/react` to 12.1.2 as it no longer supports React versions below 17.
- The mock data was out of sync with the API which meant it had to be updated.  In particular `item.price` is now `priceRange`. `priceRange.display.max` is the current price.
- Updated to `sass` as `node-sass` is no longer supported and doesn't work on Apple M1 macs. 
- Installed `identity-obj-proxy` as it was required for module mocks in Jest.
- Used `react-responsive-carousel` for the carousel as it's quicker than building a carousel from scratch.
- Added the `__mocks__` directory and the `styleMock.js` export for when we are using the css via the carousel.
- Within `package.json`, 3001 is the port that has been specified and so http://localhost:3001 needs to be used and not http://localhost:3000 

